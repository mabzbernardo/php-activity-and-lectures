<!DOCTYPE html>
<html>
<head>
	<title>Horoscope Activity</title>
	<!-- bootswatch -->
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/flatly/bootstrap.css">
</head>
<body>
	<h1 class="text-center my-5">Welcome</h1>
	<div class="col-lg-4 offset-lg-4">
		<form action="controller/registration-process.php" class="bg-light p-4" method="POST">
			<!-- POST - data sent via form ,both get and post super global variable
				GET - data sent via url.
			 -->
			<div class="form-group">
				<label for="name">Name</label>
				<input type="text" name="name" class="form-control">
			</div>
			<div class="form-group">
				<label for="birthMonth">Birth Month</label>
				<input type="number" name="birthMonth" class="form-control">
			</div>
			<div class="form-group">
				<label for="birthDay">Birth Day</label>
				<input type="number" name="birthDay" class="form-control">
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-success">Check Zodiac</button>
			</div>
</body>
</html>